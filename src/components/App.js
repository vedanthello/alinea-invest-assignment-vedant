import React from 'react';
import TopBar from "./TopBar";
import Intro from "./Intro";
import BackedBy from "./BackedBy";
import StartSaving from "./StartSaving";
import Products from "./Products";
import Box from '@material-ui/core/Box';
import Fab from '@material-ui/core/Fab';
import CommentIcon from '@material-ui/icons/Comment';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  padding: {
    padding: theme.spacing(0, 6) 
  },
  fab: {
    position: "fixed",
    bottom: "2rem",
    right: "2rem",
  }
}));

export default function App() {
  const classes = useStyles();

  return (
    <div>
      <TopBar />
      <Box className={classes.padding}>
        <Intro />
        <BackedBy />
        <StartSaving />
        <Products />
      </Box>

      <Fab color="primary" className={classes.fab}>
        <CommentIcon fontSize="large" />
      </Fab>
    </div>
  );
}