import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Button from "@material-ui/core/Button";
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';

function ElevationScroll(props) {
  const { children } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired
};

const useStyles = makeStyles((theme) => ({
  topBar: {
    padding: theme.spacing(1, 6)
  },
  logo: {
    marginRight: theme.spacing(10)
  },
  navContainer: {
    flexGrow: 1,
    "& *": {
      marginRight: theme.spacing(1.5)
    }
  },
  homeButton: {
    color: "#f6f"
  },
  loginButton: {
    marginRight: theme.spacing(3)
  }
}));

export default function TopBar(props) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <ElevationScroll {...props}>
        <AppBar color="white" position="sticky">
          <Toolbar className={classes.topBar}>

            <Box className={classes.logo}>
              <a href="https://onjuno.com/">
                <img width="150" 
                    src="https://juno-public.s3-us-west-1.amazonaws.com/on-juno%402x.png" 
                    alt="onjuno-logo" 
                />
              </a>
            </Box>
            
            <Box className={classes.navContainer}>
              <Button className={classes.homeButton}>Home</Button>
              <Button color="secondary">
                Company
                <ExpandMoreIcon />
              </Button>
              <Button color="secondary">
                Learn
                <ExpandMoreIcon />
              </Button>
              <Button color="secondary">
                Legal
                <ExpandMoreIcon />
              </Button>
            </Box>

            <Button className={classes.loginButton} variant="outlined" color="primary">
              Login
            </Button>
            <Button variant="contained" color="primary">
              Sign Up
            </Button>

          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />      
    </React.Fragment>
  );
}
