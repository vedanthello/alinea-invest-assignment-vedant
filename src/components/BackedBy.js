import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(10),
    "& h6": {
      color: "rgba(55, 55, 55)",
      fontSize: "1.1rem",
      letterSpacing: "0.25rem",
      marginBottom: theme.spacing(4)
    },
    "& img": {
      width: "12.5rem"
    }
  }
}));
  
export default function Intro() {
  const classes = useStyles();
 
  return (
    <Grid container direction="column" alignItems="center" spacing={1} className={classes.root} >
      <Grid item>
        <Typography variant="h6">
          BACKED BY THE BEST
        </Typography>
      </Grid>
      <Grid item container alignItems="center" justify="center" spacing={10}>
        <Grid item>
          <img src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/sequioa-image%403x.png" alt="sequioa" />
        </Grid>
        <Grid item>
          <img src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/polychain-capital%403x.png" alt="Polychain Capital" />
        </Grid>
        <Grid item>
          <img src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/consensys-logo%403x.png" alt="Consensys" />  
        </Grid>
      </Grid>
    </Grid>
  );
}

