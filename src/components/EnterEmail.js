import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import MailOutlineSharpIcon from '@material-ui/icons/MailOutlineSharp';
import InputBase from '@material-ui/core/Input';
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(theme => ({
  root: {
		padding: theme.spacing(2),
		border: "1px solid rgba(178, 178, 178, 0.5)",
		borderRadius: "1.125rem"
  },
  iconMail: {
    marginRight: theme.spacing(1)
  }
}));

export default function EnterEmail() {
  const classes = useStyles();

  return (
    <Grid className={classes.root} container alignItems="center">

				<Grid item className={classes.iconMail}>
					<MailOutlineSharpIcon color="primary" />
				</Grid>
        
				<Grid item>
				  <InputBase disableUnderline placeholder="Enter Email Address"  />
				</Grid>

        <Grid item>
          <Button variant="contained" color="primary">
            Join Now
          </Button>
        </Grid>        

		</Grid>
  );
}