import React from 'react';
import { Typography, makeStyles, Grid } from '@material-ui/core';

const useStyles = makeStyles({
  root:{
    marginTop: "6rem",
    "& h5": {
      marginBottom: "1rem",
      fontWeight: "600"
    },
    "& h3": {
      marginBottom: "1.5rem",
      fontFamily: `"Libre Baskerville", serif`,
      fontSize: "2.7rem"
    },
    "& p": {
      fontSize: "1.1rem",
      lineHeight: "2rem"
    },
    "& img": {
      maxWidth: "100%",
      height: "auto"
    }
  }
});

export default function StartSaving() {
  const classes = useStyles();

  return (
    <Grid container spacing={6} alignItems="center" justify="space-between" className={classes.root}>
      <Grid item xs={7} container direction="column" alignItems="flex-start" justify="space-between">
        <Grid item>
          <Typography variant="h5" color="primary">
            Start saving for a rainy day fund
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="h3">
            Use Our Checking Account to Achieve Your Financial Goals
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="body1" color="secondary">
            With the latest Federal rate cut, the largest banks are offering close to 0% APY on their checking and savings accounts. OnJuno checking will earn you more than 20x the national average*. Deposits up to $250,000 are FDIC insured through our banking partner Evolve Bank and Trust. Grow your idle money faster with our checking account and start saving towards a rainy day fund, big expense, or vacation.
          </Typography>
        </Grid>
      </Grid>
      <Grid item xs={5}>
        <img src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/svg/juno-apy-215.svg" 
             alt="Cashback" 
        />
      </Grid>
    </Grid>
  );
}

