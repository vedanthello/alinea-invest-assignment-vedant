import React from 'react';
import FirstProduct from "./FirstProduct";
import SecondProduct from "./SecondProduct";
import ThirdProduct from "./ThirdProduct";
import { Typography, makeStyles, Grid } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    marginTop: "7rem",
    marginBottom: "7rem",
  },
  heading: {
    marginBottom: "10rem",
    "& h3": {
      fontFamily: `"Libre Baskerville", serif`,
      marginBottom: "1.5rem"
    },
    "& span": {
      fontSize: "1rem",
      verticalAlign: "super"
    }
  },
  productStack: {
    "& > div:not(:last-child)": {
      marginRight: "1.5rem"
    }
  }
});

export default function Products() {
  const classes = useStyles();

  return (
    <Grid container direction="column" alignItems="center" justify="space-between" className={classes.root}>
      <Grid item container direction="column" alignItems="center" justify="space-between" className={classes.heading} >
        <Typography variant="h3">
          Sign up early to save more
        </Typography>
        <Typography variant="h5" align="center" color="secondary">
          Depending on the level of your checking account, you’ll earn<br /> between 1.15%<span>3</span> to 2.15%<span>1</span> bonus rate.
        </Typography>
      </Grid>
      
      <Grid item container className={classes.productStack} wrap="nowrap" alignItems="center" justify="space-between">
        <FirstProduct />
        <SecondProduct />
        <ThirdProduct />
      </Grid>
    </Grid>
  );
}

