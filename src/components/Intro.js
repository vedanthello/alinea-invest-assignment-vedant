import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import EnterEmail from "./EnterEmail";
import logoFDIC from "../logoFDIC.png";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  heading: {
    fontFamily: "'Libre Baskerville', serif",
    color: "rgb(3, 2, 0)"
  },
  description:{
    lineHeight: "2.25rem",
    letterSpacing: "normal"
  },
  priorityAccess: {
    display: "flex",
    marginLeft: "0.625rem",
    "& img": {
      marginRight: "0.8125rem"
    }
  },
  ContainerFDIC: {
    marginTop: theme.spacing(4),
    "& img": {
      marginTop: "0.3rem",
      marginRight: "0.8125rem"
    }
  },
  mobileAppImage: {
    "& > img": {
      maxWidth: "100%",
      height: "auto"
    }
  }
}));

export default function Intro() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container alignContent="center" alignItems="center" justify="space-between">

        <Grid item container spacing={4} direction="column" alignItems="flex-start" xs={7}>
          <Grid item>
            <Typography className={classes.heading} variant="h2">
              The Most Powerful Checking Account
            </Typography>
          </Grid>
          <Grid item>
            <Typography color="secondary" variant="h5" className={classes.description}>
              Our checking account gives you higher returns<br /> than a savings account with no hidden fees.
            </Typography> 
          </Grid>
          <Grid item>
            <EnterEmail />
          </Grid>
          <Grid item className={classes.priorityAccess}>
            <img width="30" 
                 src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/createaccount-homeaddress-flag%403x.png" 
                 alt="USA Flag" 
            />
            <Typography color="secondary">
              277 spots left for Priority Access
            </Typography>
          </Grid>
          <Grid item container alignItems="center" className={classes.ContainerFDIC}>
            <Grid item>
              <img src={logoFDIC} alt="FDIC logo" />
            </Grid>
            <Grid item>
              <Typography variant="caption" color="secondary">
                Banking Services Provided By<br /> Evolve Bank &amp; Trust; Member FDIC
              </Typography>
            </Grid>
          </Grid>  
        </Grid>

        <Grid item xs={5}>
          <Box className={classes.mobileAppImage}>
            <img src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/juno-mobile-app%403x.png" 
                 alt="juno-mobile-app" 
            />
          </Box>
        </Grid>

      </Grid>
    </div>
  );
}