import React from 'react';
import { Typography, makeStyles, Grid, Box, Button, Divider } from '@material-ui/core';
import { List, ListItem, ListItemText,  ListItemIcon } from '@material-ui/core';
import greyTick from "../greyTick.png";
import greyDollar from "../greyDollar.png";

const useStyles = makeStyles({
  root: {
    border: "1px solid #EEE",
    boxShadow: "5px 18px 64px 0 rgba(0, 0, 0, 0.1)",
    borderRadius: "0.625rem"
  },
  firstContainer: {
    textAlign: "center",
    backgroundColor: "#f7f7fb",
    paddingLeft: "1rem",
    paddingRight: "1rem",
    paddingBottom: "2.125rem",
    "& img": {
      width: "80%",
      height: "auto",
      position: "relative",
      left: "10%",
      bottom: "3.75rem",
      display: "block"
    }
  },
  cardType: {
    marginTop: "-2rem",
    fontSize: "1.1rem",
    fontWeight: "500",
    color: "rgba(55, 55, 55, 0.8)",
    letterSpacing: "4px"
  },
  cardPercent: {
    marginTop: "-0.8rem",
    fontSize: "3.375rem",
    fontWeight: "600",
    letterSpacing: "-0.1rem",
    "& span": {
      verticalAlign: "text-top",
      fontSize: "1.5rem",
      color: "rgba(178, 178, 178)",
      marginLeft: "-0.2rem"
    }
  },
  cashback: {
    marginTop: "1rem",
    "& span:first-child": {
      color: "white",
      padding: "0.25rem 0.4rem",
      backgroundColor: "rgba(178, 178, 178)",
      borderRadius: "0.3125rem",
      fontSize: "1.25rem",
      fontWeight: "800"
    },
    "& span:nth-child(2)": {
      fontSize: "1.125rem",
      color: "rgba(118, 118, 118)",
      marginLeft: "0.5rem",
      letterSpacing: "0.05rem"
    }
  },
  secondContainer: {
    paddingLeft: "2.125rem",
    paddingRight: "2.125rem",
    marginTop: "1.6rem",
    marginBottom: "1.4rem",
    "& p:nth-child(1) span:nth-child(1)": {
      letterSpacing: 0,
      color: "rgba(0, 168, 107)",
      fontSize: "1.25rem",
      fontWeight: "700"
    },
    "& p:nth-child(1) span:nth-child(2)": {
      letterSpacing: 0,
      color: "rgba(118, 118, 118)",
      fontSize: "0.875rem",
      fontWeight: "900"
    },
    "& p:nth-child(1) span:nth-child(3)": {
      letterSpacing: 0,
      color: "rgba(118, 118, 118)",
      fontSize: "0.875rem",
      fontWeight: "400"
    },
    "& p:nth-child(2) span:nth-child(1)": {
      letterSpacing: 0,
      color: "rgba(118, 118, 118)",
      fontSize: "0.75rem",
      fontWeight: "700"      
    },
    "& p:nth-child(2) span:nth-child(2)": {
      letterSpacing: 0,
      color: "rgba(118, 118, 118)",
      fontSize: "0.75rem",
      fontWeight: "400"      
    }
  },
  grapf: {
    marginTop: "0.5rem",
    width: "100%",
    height: "0.625rem",
    display: "flex",
    borderRadius: "0.3125rem",
    backgroundColor: "rgba(238, 238, 238)",
    "& div": {
      width: "88.96%",
      backgroundImage: "linear-gradient(180deg, #00db8c, #00a86b)",
      height: "100%",
      borderRadius: "0.3125rem"
    }
  },
  wrapperButton: {
    marginTop: "1.5rem",
    width: "100%",
    height: "3.75rem",
    "& button": {
      width: "100%",
      fontSize: "1.25rem",
      fontWeight: "700",
      height: "100%"
    }
  },
  thirdContainer: {
    paddingLeft: "2.125rem",
    paddingRight: "2.125rem",
    marginTop: "1.6rem",
    marginBottom: "1.4rem",
    "& h6": {
      fontSize: "1.125rem",
      letterSpacing: "0.25rem",
      opacity: "0.5"
    }
  },
  features: {
    marginTop: "1rem",
    "& span span": {
      verticalAlign: "super",
      fontSize: "0.875rem"
    }
  },
  additionalFeature: {
    height: "6rem",
    marginTop: "0.5rem",
    fontWeight: "400",
    fontSize: "0.875rem",
    lineHeight: "1.8",
    "& span": {
      fontWeight: "700"
    }
  },
  fourthContainer: {
    padding: "1.6rem 2.125rem 1.4rem 2.125rem",
    borderBottomLeftRadius: "0.625rem",
    borderBottomRightRadius: "0.625rem",
    backgroundColor: "#f7f7fb",
    "& div:first-child": {
      marginRight: "1rem"
    },
    "& p:nth-child(1)": {
      color: "rgba(178, 178, 178)",
      // textDecoration: "line-through",
      fontSize: "1rem",
      fontWeight: "500"
    },
    "& p:nth-child(2)": {
      color: "rgba(178, 178, 178)",
      fontSize: "1.5rem",
      fontWeight: "500"
    },
    "& p:nth-child(3)": {
      color: "rgba(6, 135, 88)",
      fontSize: "1rem",
      fontWeight: "500"
    }
  }
});

export default function FirstProduct() {
  const classes = useStyles();

  return (
    <Grid xs={4} className={classes.root}>
      <Box className={classes.firstContainer}>
        <img src="https://juno-public.s3-us-west-1.amazonaws.com/Juno/juno-basic%403x.png" 
             alt="card" 
        />
        <Typography className={classes.cardType}>BASIC</Typography>
        <Typography className={classes.cardPercent}>
          1.15%<span>3</span>
        </Typography>
        <Typography className={classes.cashback}>
          <span>3%</span>
          <span>cash back</span>
        </Typography>
      </Box>

      <Box className={classes.secondContainer}>
        <Grid container justify="space-between" alignItems="flex-end">
          <Typography component="p">
            <span></span>
            <span>10000 </span>
            <span>spots</span>
          </Typography>
          <Typography component="p">
            <span>10000 </span>
            <span>spots left </span>
          </Typography>
        </Grid>

        <Box className={classes.grapf}>
          {/* <Box></Box> */}
        </Box>

        <Box className={classes.wrapperButton}>
          <Button disabled variant="contained" color="secondary">Coming Soon</Button>
        </Box>

      </Box>

      <Divider />

      <Box className={classes.thirdContainer}>
        <Typography variant="h6" color="secondary">INCLUDES</Typography>

        <List disablePadding className={classes.features}>
          <ListItem disableGutters>
            <ListItemIcon>
              <img src={greyTick} alt="grey tick" />
            </ListItemIcon>
            <ListItemText>
              Free Debit Card
            </ListItemText>
          </ListItem >

          <ListItem disableGutters>
            <ListItemIcon>
              <img src={greyTick} alt="grey tick" />
            </ListItemIcon>
            <ListItemText>
              1.15%<span>3</span> Bonus Rate Checking Account
            </ListItemText>
          </ListItem >
          
          <ListItem disableGutters>
            <ListItemIcon>
              <img src={greyTick} alt="grey tick" />
            </ListItemIcon>
            <ListItemText>
              3% Cash back on brands you love
            </ListItemText>
          </ListItem >
          
          <ListItem disableGutters>
            <ListItemIcon>
              <img src={greyTick} alt="grey tick" />
            </ListItemIcon>
            <ListItemText>
              Free Cash Withdrawals
            </ListItemText>
          </ListItem >
          
          <ListItem disableGutters>
            <ListItemIcon>
              <img src={greyTick} alt="grey tick" />
            </ListItemIcon>
            <ListItemText>
              Phone &amp; Chat Support
            </ListItemText>
          </ListItem >          
        </List>

        <Typography component="p" align="left" color="secondary" className={classes.additionalFeature}>
          Fund your account with a minimum opening deposit of <span>$0.</span>
        </Typography>

      </Box>

      <Grid container alignItems="flex-start" className={classes.fourthContainer}>
        <Box>
          <img src={greyDollar} alt="dollar" />
        </Box>
        <Box>
          <Typography component="p">No Membership Fee</Typography>
          <Typography component="p">Free Forever!</Typography>
          <Typography component="p"><br /></Typography>
        </Box>
      </Grid>

    </Grid>
  );
}

