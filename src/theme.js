import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#4643ee"
    },
    secondary: {
      main: "rgba(118, 118, 118)"
    }
  },
  typography: {
    button: {
      textTransform: "none",
      fontSize: "1rem"
    }
  },
  overrides: {
    MuiListItemIcon: {
      root: {
        minWidth: "initial",
        marginRight: "0.7rem"
      },
    },
  }
});

export default theme;